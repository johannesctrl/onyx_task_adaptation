const path = require('path')
const {EnvironmentPlugin} = require("webpack")
const { merge } = require("webpack-merge")
const common = require("./webpack.common")

module.exports = merge(common, {
  mode: 'production',
  output: {
    filename: '[name].prod.bundle.js',
  },
  plugins: [
    new EnvironmentPlugin({
      OAUTH_URL: "https://ki-gateway.tech4comp.dbis.rwth-aachen.de/auth-server/",
      LRS_REQUESTS_SERVER_URL: "https://ki-gateway.tech4comp.dbis.rwth-aachen.de/lrs-requests-server/"
    })
  ]
})