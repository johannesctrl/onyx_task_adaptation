
const get_onyx_variable = function() {
    /**
     * extracts the data from the onyx variable
     */
    let onyx_variable = document.getElementById("change_variables").innerHTML
    onyx_variable = JSON.parse(onyx_variable)
    
    return onyx_variable
}

const change_input_fields = function(a, b) {
    /**
     * applies the value to the fields
     */
    const xdw_a = document.getElementById("xdw_a").getElementsByTagName('input')[0]
    xdw_a.value = a
    xdw_a.readOnly = true

    const xdw_b = document.getElementById("xdw_b").getElementsByTagName('input')[0] 
    xdw_b.value = b
    xdw_b.readOnly = true
}

export const adapt_to_level = function (level) {
    /**
     * defines which values of the onyx variable to use, according to the level
     */
    console.log("level =", level)
    const variables_all_levels = get_onyx_variable()
    const variables_single_level = variables_all_levels[level]
    const a = variables_single_level[0]
    const b = variables_single_level[1]
    change_input_fields(a, b)
}