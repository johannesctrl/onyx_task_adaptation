import "./overlay.css"
import { get_level_of_math } from "./get_category_level"

export const create_overlay = function() {

    // overlay
    const div_overlay = document.createElement("div")
    document.body.appendChild(div_overlay)
    div_overlay.setAttribute("id", "overlay")
    
    // form
    div_overlay.innerHTML = `
        <div class="overlay-content">
            <div class="oAuthForm">
                <p>oAuthForm</p>
                <label for="email">email address</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                <label for="password">password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
            </div>
        </div>
    `
    
    /*
    const customStudyIdForm = document.getElementsByClassName("customStudyIdForm")[0]
    const btn_customStudyIdForm = document.createElement("button")
    btn_customStudyIdForm.innerHTML += "Submit"
    btn_customStudyIdForm.addEventListener("click", function() { alert(document.getElementById("custom-study-id").value) })
    customStudyIdForm.appendChild(btn_customStudyIdForm)
    */
    const oAuthForm = document.getElementsByClassName("oAuthForm")[0]
    const btn_get_study_id = document.createElement("button")
    btn_get_study_id.innerHTML += "Login"
    btn_get_study_id.addEventListener("click", function() { get_study_id(process.env.OAUTH_URL + "postStudyId") })
    oAuthForm.appendChild(btn_get_study_id)

    const btn_sign_up = document.createElement("button")
    btn_sign_up.innerHTML += "Create account"
    btn_sign_up.addEventListener("click", function() { open_window(process.env.OAUTH_URL + "sign-up") })
    oAuthForm.appendChild(btn_sign_up)
}

// window
const open_window = function(url) {
    window.open(url, "_blank", "location=yes, height=570, width=520, scrollbars=yes, status=yes") 
}
const on = function() {
    document.getElementById("overlay").style.display = "block"
}
export const off = function() {
    console.log("off")
    document.getElementById("overlay").style.display = "none"
}
// functions for buttons
const send_auth_request = function(url) {
    console.log("POST...")
    const promise = new Promise((resolve, reject) => {

        const client_id = document.getElementById('email').value
        const client_secret = document.getElementById('password').value
        const authorization_basic = window.btoa(client_id + ':' + client_secret)

        const xhr = new XMLHttpRequest()
        xhr.open("POST", url)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        xhr.setRequestHeader('Authorization', 'Basic ' + authorization_basic)
        xhr.setRequestHeader('Accept', 'application/json')
        xhr.setRequestHeader('Access-Control-Allow-Origin', process.env.OAUTH_URL)
        xhr.setRequestHeader = ('Content-Type', 'text/plain')
        xhr.onload = function() { //onload -> wenn Response kommt
            if(xhr.status >= 400) {
                reject(xhr.response)
            }
            resolve(xhr.response)
        }
        xhr.onerror = function() {
            reject("Keine Antwort vom Server :-(")
        }
        xhr.send("payload test")
    })
    return promise
}

const get_study_id = function(url) {
    send_auth_request(url).then(responseData => {
        console.log("responseData = " + responseData)
        if(responseData != "false") {
            console.log("Right login data")
            localStorage.setItem("studyId", responseData)
            get_level_of_math()
        } else {
            console.log("else: Wrong login data")
        }
    }).catch(err => {
        alert(err)
        alert("catch: Wrong login data")
        console.log(err)
    })
}
