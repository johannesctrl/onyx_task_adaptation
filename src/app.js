import { create_overlay } from "./open_window"

const main = function() {
    localStorage.setItem("studyId", "")  // reset studyId
    create_overlay()
}

window.onload = function() {
    main()
}
