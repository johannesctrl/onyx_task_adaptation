import { adapt_to_level } from "./change_variables"
import { off } from "./open_window"

const send_get_http_request = function(url) {
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", url)
        xhr.responseType = "text"
        xhr.onload = function() { //onload -> wenn Response kommt (auch bei "Fail"-Response)
            resolve(xhr.response)
        }
        xhr.onerror = function() { //onerror -> wenn gar keine Response kommt
            reject("No response from the server :-(")
        }
        xhr.send()
    })
    return promise
}

const get_category_level = function(study_id, category_name) {
    const url = process.env.LRS_REQUESTS_SERVER_URL + "get-category-level?student-id=" + String(study_id) + "&category-name=" + String(category_name)
    console.log(url)

    send_get_http_request(url).then(responseData => {
        console.log("success!")
        console.log(responseData)
        const json_obj = JSON.parse(responseData)
        const category_level = json_obj["math"]
        console.log("category_level = ", String(category_level))
        adapt_to_level(String(category_level))
        off()
        return category_level
    }).catch(err => {
        console.log("error!")
        console.log(err)
        window.alert("Level of the requested user and category not found.")
    })
}

export const get_level_of_math = function() {
    const study_id = localStorage.getItem("studyId");
    const math_level = get_category_level(study_id, "math")

    return math_level
}