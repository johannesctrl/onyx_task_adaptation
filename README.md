# onyx-task-adaptation
This project is a prototype to adapt the difficulty of an ONYX task to the level of the user. It does so by checking the user's identity, then reading the level of the user and then change the ONYX task. The project can be embedded into an ONYX test as a JavaScript file. The user authentificates with the project [auth-server](https://gitlab.com/johannesctrl/auth-server/) and asks for the user's level stored in an LRS by using the [lrs-requests-server](https://gitlab.com/johannesctrl/lrs-requests-server/).

In the following picture you can see the dataflow diagram.
![](image_1.jpg)
