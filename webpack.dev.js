const path = require('path')
const {EnvironmentPlugin} = require("webpack")
const { merge } = require("webpack-merge")
const common = require("./webpack.common")

module.exports = merge( common, {
  mode: 'development',
  output: {
    filename: '[name].dev.bundle.js',
  },
  plugins: [
    new EnvironmentPlugin({
      OAUTH_URL: "http://127.0.0.1:5000/",
      LRS_REQUESTS_SERVER_URL: "http://127.0.0.1:12345/"
    })
  ]
})